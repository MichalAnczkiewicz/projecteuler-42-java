package Michal;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class TriangularNumber {

    public static void main(String[] args) throws FileNotFoundException {
        File file = new File("dane.txt");
        Scanner input = new Scanner(file);
        String answer = input.next();
        String[] words = answer.replaceAll("\"", "").split(",");
        int wordValue;
        int wordCount = 0;
        for (int i = 0; i < words.length; i++) {
            wordValue = getWordValue(words[i]);
            if (isTriangular(wordValue)) {
                System.out.println(words[i]);
                wordCount++;
            }

        }
        System.out.println("Total amount of triangle words is: " + wordCount);

    }

    public static boolean isTriangular(int number) {
        int sqrt = (int) Math.sqrt(number * 2);
        return sqrt * (sqrt + 1) == number * 2;
    }

    public static int getWordValue(String word) {
        int total = 0;
        for (int i = 0; i < word.length(); i++) {
            total += word.charAt(i) - 64;
        }
        return total;
    }


}

